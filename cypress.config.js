const { defineConfig } = require("cypress");

module.exports = defineConfig({
  e2e: {
    setupNodeEvents(on, config) {
      // implement node event listeners here
    },
    baseUrl: "https://kp.haieronline.ru/kp/Q29tbWVyY2lhbFByb3Bvc2FsVHlwZTo2",
  },
  // Добавляем команду для изменения разрешения экрана
  viewportWidth: 1920,
  viewportHeight: 1080,

  experimentalsessionAndOrigin: true,
});
