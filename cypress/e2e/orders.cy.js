describe("Страница раздела 'Оформить заказ'", () => {
  beforeEach(() => {
    cy.visit("/");
  });

  it("Test 1: При нажатии на логотип открывается главная страница КП  ", () => {
    cy.get(".el-button.el-button--primary").eq(1).click(); //нажать на кнопку "оформить заказ" в блоке версии
    cy.get(".kp-header__logo").eq(0).click(); //нажать на логотип для открытия главной страницы
    cy.get(".col-12").should("exist").should("be.visible"); //отображение блоков главной страницы
  });

  it("Test 3: Скачать Договор, приложения и прочие документы  ", () => {
    cy.get(".el-button.el-button--primary").eq(1).click(); //нажать на кнопку "оформить заказ" в блоке версии
    cy.get("a").contains("Договор купле-продажи кухни").click();
    cy.readFile("cypress/downloads/6566fb62b1d69dogovor-kupli-prodaji-kuhni_har-2023-osnovnoy.docx", "binary").should("exist"); // Замените 'file.pdf' на имя вашего скачанного файла
  });
});
