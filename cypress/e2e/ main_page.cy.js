describe("Страница раздела 'Главная страница'", () => {
  beforeEach(() => {
    cy.visit("/");
  });

  it("Test 2: При нажатии на версию выбирается одна из версий  ", () => {
    cy.get(".swiper-wrapper .swiper-slide.kp-prject-variants__slide")
      .eq(3)
      .click();
    cy.get(".kp-prject-variants__current.text-s.color_text-tertiary")
      .eq(1)
      .contains("Версия: 1")
      .should("exist")
      .should("be.visible");
    cy.get(".swiper-wrapper .swiper-slide.kp-prject-variants__slide")
      .eq(4)
      .click();
    cy.get(".kp-prject-variants__current.text-s.color_text-tertiary")
      .eq(1)
      .contains("Версия: 2")
      .should("exist")
      .should("be.visible"); //отобразился номер 2 версии
  });

  it("Test 3: При нажатии на 'Подробные характеристики' открывается модальное окно с характеристиками ", () => {
    cy.get(".el-button.is-text.hidden-lg-and-down").click(); //нажать на кнопку "Подробные характеристики"
    cy.get(".fs-on-mobile-dialog__body").should("exist").should("be.visible"); //открылось модальное окно
  });

  it("Test 4: При нажатии на кнопку 'Оформить заказ' в блоке с версией открывается страница 'Оформление заказа'", () => {
    cy.get(".el-button.el-button--primary").eq(1).click(); //нажать на кнопку "оформить заказ" в блоке версии
    cy.url().should(
      "eq",
      "https://kp.haieronline.ru/kp/Q29tbWVyY2lhbFByb3Bvc2FsVHlwZTo2/checkout"
    ); // проверяем, что URL изменился на нужный
    cy.get(".kp-checkout-main__content.d-lg-flex.align-items-start")
      .should("exist")
      .should("be.visible"); // блок для заполнение данных видимый на странице
  });

  it("Test 5: При нажатии на кнопку 'Записаться в салон' в блоке версий открывается модальное окно 'Запись в салон'", () => {
    cy.get(".el-button.el-button--large.is-plain").click(); //нажать на кнопку "Записаться в салон" в блоке версии
    cy.get(".el-dialog.kp-request-dialog").should("exist").should("be.visible"); //открылось модальное окно "Запись в салон"
    cy.get(".kp-request-dialog__body").should("exist").should("be.visible"); // блок для заполнения данных видимый в модальном окне
  });

  it("Test 8: При нажатии на стрелки сладера в блоке 'Горячие предложения по технике' отображаются разные предложения ", () => {
    cy.contains("Автоподбор для версии '20231026-CB31564").should("be.visible"); //текст на первом слайдере
    cy.get(".el-button.is-circle").eq(5).click(); //нажать на стрелку слайдера
    cy.contains("Автоподбор для версии '20231026-CB31565").should("be.visible"); //текст на втором слайдере
  }); //TODO после создания тествого КП изменить текс на слайдерах

  // it("Test 9: При нажатии на кнопку '+Оформить заказ' в блоке 'Горячие предложения по технике' открывается страница сайта Haier", () => {
  //cy.window().then((win) => {
  // cy.stub(win, "open", (url) => {
  // win.location.href = url;
  //}).as("winOpen");
  // }); //преопределяю команды open в джаваскрипт
  //cy.get(".el-button.el-button--primary").eq(2).click(); //нажать на кнопку "+Оформить заказа"
  //cy.contains("Корзина").should("exist").should("be.visible");
  // });
});
